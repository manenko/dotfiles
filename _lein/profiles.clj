 {:repl   {:plugins      [[cider/cider-nrepl         "0.28.4"]
                          [refactor-nrepl            "3.5.2"]
                          [mx.cider/enrich-classpath "1.9.0"]]}

  :reveal {:dependencies [[dev.vlaaad/reveal-pro "1.3.344"]]
           :jvm-opts     ["-Dvlaaad.reveal.prefs={:theme,:light}"]}}
