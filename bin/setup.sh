#!/usr/bin/env bash

function link_file {
    source="${PWD}/$1"
    target="${HOME}/${1/_/.}"

    if [ -e "${target}" ]; then
        mv -i "$target" "${target}.old"
    fi

    ln -sf "${source}" "${target}"
}

for i in _*
do
  link_file "$i"
done

git pull --recurse-submodules
git submodule update --init --recursive

