#!/usr/bin/env bash

# Install
# Install haskell stack
# $ curl -sSL https://get.haskellstack.org/ | sh
# OR $ wget -qO- https://get.haskellstack.org/ | sh
# stack setup --install-ghc


# TODO:
# Implement rest of setup in haskell script(-s)


# TODO:
# There are different types of config files.
# Some of them should live in the home directory (.emacs.d, .vimrc, .gitconfig).
# Others have to be installed into some subfolder, i.e.:
# ~/.local/share/fonts/
# ~/.config/i3/


# TODO: Use different gitignore files for different folders?
