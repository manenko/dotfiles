(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("835d934a930142d408a50b27ed371ba3a9c5a30286297743b0d488e94b225c5f"
     "835868dcd17131ba8b9619d14c67c127aa18b90a82438c8613586331129dda63" default))
 '(line-spacing nil)
 '(safe-local-variable-values
   '((lsp-rust-analyzer-import-granularity . "item")
     (eval add-to-list 'cider-jack-in-nrepl-middlewares
      "vlaaad.reveal.nrepl/middleware")
     (cider-known-endpoints ("apptopia/vpc-research" "localhost" "34708"))))
 '(writeroom-width 100))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-doc-face ((t (:inherit aircon-goblin))))
 '(treemacs-root-face ((t (:inherit font-lock-string-face :weight bold :height 160 :family "Iosevka Term")))))
