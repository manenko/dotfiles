;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(setq user-full-name    "Oleksandr Manenko"
      user-mail-address "koornacht@use.startmail.com")

(add-to-list 'load-path "~/.doom.d/lisp")
(require 'om/c-utils)
(require 'om/path)
(require 'verba/c-section-comment)

(setq doom-font
      (font-spec :family "Menlo" :size 13 :weight 'normal))

(setq doom-themes-neotree-enable-variable-pitch nil
      doom-themes-neotree-file-icons            nil
      doom-themes-neotree-project-size          1.0
      doom-themes-line-spacing                  1.0
      doom-themes-neotree-folder-size           0.9)

;; (setq doom-theme 'leuven)
;; (setq doom-theme 'acme)
;; (setq doom-theme 'xcode-light)
;; (setq doom-theme 'aircon)
;; (setq doom-theme 'xcode-dark)
;; (setq doom-theme 'nothing)
(setq doom-theme 'doom-dark+)

;; (require 'auth-source-pass)
;; (setq auth-sources '("~/.authinfo"))
;; (auth-source-pass-enable)

(setq display-line-numbers-type 'relative)

(setq lsp-ui-doc-enable nil)
(setq lsp-enable-symbol-highlighting nil)

(setq line-spacing nil)

(setq +zen-text-scale 1)

;; (add-to-list 'initial-frame-alist '(fullscreen . maximized))
(add-to-list 'default-frame-alist '(height .  55))
(add-to-list 'default-frame-alist '(width  . 102))

(add-hook! prog-mode #'display-fill-column-indicator-mode)

(add-hook! c-mode
  (c-set-style  "k&r")
  (c-set-offset 'inextern-lang 0)
  (setq-local c-basic-offset    2
              indent-tabs-mode nil
              fill-column      78))

(add-hook! rust-mode
  (setq-local fill-column              80
              tab-width                4
              rust-indent-offset       4
              indent-tabs-mode         nil
              ;; rust-format-on-save      t
              ))

(setq lsp-clients-clangd-args '("-j=8"
                                "--background-index"
                                "--clang-tidy"
                                "--completion-style=detailed"
                                "--header-insertion=never"
                                "--header-insertion-decorators=0"))
(setq rustic-rustfmt-args "+nightly")
(setq rustic-format-trigger 'on-save)
(setq +format-with-lsp nil)
(after! lsp-clangd (set-lsp-priority! 'clangd 2))

(after! sly
  (setq sly-complete-symbol-function 'sly-flex-completions
        sly-lisp-implementations '((sbcl ("sbcl") :coding-system utf-8-unix)
                                   (ecl  ("ecl")  :coding-system utf-8-unix)))
  (set-popup-rules!
    '(("^\\*sly-mrepl"
       :side  left
       :width 100
       :quit  nil
       :ttl   nil))))

;; (custom-theme-set-faces
;;    'acme
;;    '(font-lock-doc-face
;;      ((t (:foreground "#005500" :italic nil))))
;;    '(highlight-doxygen-comment
;;      ((t (:foreground "#005500" :italic nil)))))

(remove-hook 'doom-first-buffer-hook #'global-hl-line-mode)

(after! ispell
  (setenv "LANG" "en_GB.UTF-8")
  ;; (setenv "DICPATH" (expand-file-name "~/Library/Spelling"))
  (setq ispell-personal-dictionary "~/.hunspell_personal"))

(after! cider
  (setq
   cider-repl-prompt-function #'cider-repl-prompt-abbreviated
   cider-repl-result-prefix   ""
   cider-print-fn             'pprint)
  (set-popup-rules!
    '(("^\\*cider-repl"
       :side  left
       :width 100
       :quit  nil
       :ttl   nil))))

;; accept completion from copilot and fallback to company
(use-package! copilot
  ;; :hook (prog-mode . copilot-mode)
  :bind (:map copilot-completion-map
              ("<tab>"   . #'copilot-accept-completion)
              ("TAB"     . #'copilot-accept-completion)
              ("C-TAB"   . #'copilot-accept-completion-by-word)
              ("C-<tab>" . #'copilot-accept-completion-by-word))
  :config
  (add-to-list 'copilot-indentation-alist '(lisp-mode 2))
  (add-to-list 'copilot-indentation-alist '(emacs-lisp-mode 2)))

(use-package! asdf
  :config
  (asdf-enable))

(use-package! rustic
  :custom
  (rustic-analyzer-command '("rustup" "run" "stable" "rust-analyzer")))

;; Doom configures auth-sources by default to include the Keychain on macOS, but
;; it puts it at the beginning of the list. This causes creation of auth items
;; to fail because the macOS Keychain sources do not support creation yet.
;; I reverse it to leave ~/.authinfo.gpg at the beginning.
;;
;; https://zzamboni.org/post/my-doom-emacs-configuration-with-commentary/
(after! auth-source
  (setq auth-sources (nreverse auth-sources)))

(use-package! dap-mode
  :config
  (require 'dap-lldb)
  (require 'dap-gdb-lldb)
  (dap-gdb-lldb-setup))

(use-package! lispy
  :config
  (setq lispy-colon-no-space-regex
        '((lisp-mode . "")
          (slime-repl-mode . ""))))
