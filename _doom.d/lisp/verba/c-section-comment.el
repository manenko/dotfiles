;;; c-section-comment.el --- Insert section-style C comments -*- lexical-binding: t; -*-
;;
;; Author: RPLACX
;; Date: 2024-09-20

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file provides a function to insert section-style comments in C code.

;;; Code:

(defun verba/insert-c-section-comment (input-string)
  "Transform INPUT-STRING and insert it as a section-style C comment.

If the current line has text, insert the section comment on a new line
below.  The comment is formatted with spaces between characters and filled with
slashes to reach the `fill-column`."
  (interactive "sEnter section title: ")
  (let ((transformed (concat "// "
                             (mapconcat (lambda (word)
                                          (mapconcat 'identity (split-string word "" t) " "))
                                        (split-string (upcase input-string)) "  ")
                             " ")))
    (setq transformed (concat transformed (make-string (- fill-column (length transformed)) ?/)))
    (if (looking-at "^$")
        (insert transformed)
      (end-of-line)
      (newline)
      (insert transformed))))

(provide 'verba/c-section-comment)

;;; c-section-comment.el ends here
