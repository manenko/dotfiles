;;; c-utils.el  --- C utilities -*- lexical-binding: t; -*-

;; Copyright (c) 2008-2021  Oleksandr Manenko

;; Author: Oleksandr Manenko
;; URL: https://gitlab.com/manenko/dotfiles/

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'om/path)

(defun om:c-header-p (file-name)
  "Return non-nil if the given FILE-NAME is a C header file."
  (string-suffix-p ".h" file-name))

(defun om:c-source-p (file-name)
  "Return non-nil if the given FILE-NAME is a C source file."
  (string-suffix-p ".c" file-name))

(defun om:c-header-path-sans-include (&optional file-name)
  "Return a path to the header file relative to include dir.

If FILE-NAME is nil, use current buffer's file name.

If the file is not a header, return nil."
  (when-let* ((header (or file-name (buffer-file-name)))
              ((om:c-header-p header)))
    (om:path-after-component header "include")))

(defun om:c-source-path-sans-src (&optional file-name)
  "Return a path to the header file relative to source dir.

If FILE-NAME is nil, use current buffer's file name.

If the file is not a C source file, return nil."
  (when-let* ((source (or file-name (buffer-file-name)))
              ((om:c-source-p source)))
    (om:path-after-component source "\\(src\\|source\\)")))

(defun om:c-header-guard-macro-name (&optional path)
  "Return a header-guard macro name from the given header PATH."
  (when-let* ((header  (om:c-header-path-sans-include path))
              (pattern (format "\\(%s\\|-\\|\\.\\)"
                               om:+path-separator+)))
    (concat (thread-last header
              (replace-regexp-in-string pattern "_" header)
              (upcase))
            "_INCLUDED")))

(defun om:c-project-name (&optional path)
  "Return project name derived from the source/header file PATH."
  (when-let ((file (or (om:c-header-path-sans-include path)
                       (om:c-source-path-sans-src     path))))
    (thread-first file
      (om:path-components)
      (seq-first)
      (file-name-sans-extension))))

(provide 'om/c-utils)

;;; c-utils.el ends here
