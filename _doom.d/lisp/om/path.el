;;; path.el  --- Path utilities.    -*- lexical-binding: t; -*-

;; Copyright (c) 2008-2021  Oleksandr Manenko

;; Author: Oleksandr Manenko
;; URL: https://gitlab.com/manenko/dotfiles/

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'cl-lib)

(defconst om:+path-separator+ "/"
  "A standard path separator.")

(cl-defun om:path-components (path)
  "Return list of PATH components."
  (split-string path om:+path-separator+ t))

(cl-defun om:path-from-components (components)
  "Return a path created from its COMPONENTS."
  (string-join components om:+path-separator+))

(cl-defun om:path-components-after (components component)
  "Return path COMPONENTS that come after the COMPONENT.

COMPONENT could be a regexp."
  (thread-last components
               (seq-reverse)
               (seq-take-while (lambda (c)
                                 (not (string-match-p component c))))
               (seq-reverse)))

(cl-defun om:path-after-component (path component)
  "Return sub path of the PATH that comes after COMPONENT.

Return PATH if COMPONENT is nil or empty string.

Return PATH if COMPONENT is not in the PATH.

COMPONENT could be a regexp."
  (when path
    (if (and component
             (not (string-empty-p component)))
        (thread-first path
                      (om:path-components)
                      (om:path-components-after component)
                      (om:path-from-components))
      path)))


(provide 'om/path)

;;; path.el ends here
