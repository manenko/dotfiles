;; -*- no-byte-compile: t; -*-

(package! doom-snippets :ignore t)
;; (package! acme-theme)
;; (package! clang-format)
(package! sql-cassandra)

(package! copilot
  :recipe (:host github :repo "copilot-emacs/copilot.el" :files ("*.el")))

(package! xcode-theme
  :recipe (:host github :repo "juniorxxue/xcode-theme" :files ("*.el")))

(package! asdf
  :recipe (:host github :repo "tabfugnic/asdf.el" :files ("*.el" "bin")))

;; (package! aircon-theme)

;; (package! mindre-theme
;;   :recipe (:host github :repo "erikbackman/mindre-theme" :files ("*.el")))

;; (package! eink-emacs
;;   :recipe (:host github :repo "maio/eink-emacs" :files ("*.el")))

;; (package! nothing
;;   :recipe (:host github :repo "jaredgorski/nothing.el" :files ("*.el")))

(package! gptel)

(package! adoc-mode)
