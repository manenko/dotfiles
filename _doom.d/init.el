;;; init.el -*- lexical-binding: t; -*-

(doom! :completion
       (company +childframe)
       (vertico +icons)

       :ui
       doom
       ;; doom-quit
       hl-todo
       modeline
       ophints
       (popup +defaults)
       vc-gutter
       vi-tilde-fringe
       workspaces
       zen

       :editor
       (evil +everywhere)
       ;; file-templates
       (format +onsave)
       lispy
       snippets
       multiple-cursors

       :emacs
       (dired +icons)
       electric
       (ibuffer +icons)
       undo
       vc

       :term
       vterm

       :checkers
       (syntax +childframe)
       (spell +flyspell +hunspell)

       :tools
       (docker +lsp)
       (eval +overlay)
       (lookup +docsets)
       lsp
       (magit +forge)
       (debugger +lsp)

       :os
       (:if IS-MAC macos)
       (tty +osc)

       :lang
       (cc +lsp)
       (clojure +lsp)
       common-lisp
       data
       emacs-lisp
       (json +lsp)
       markdown
       (org +pretty)
       rst
       (rust +lsp)
       sh
       yaml

       :config
       (default +bindings +smartparens))
